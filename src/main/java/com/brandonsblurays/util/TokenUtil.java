package com.brandonsblurays.util;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class TokenUtil {
    private static final String AUTH_HEADER = "Authorization";
    private static final String jwtSecret = System.getenv("brandonsbluraysecret");

    public static String createJwtToken(String accountId) {
            Map<String, Object> claims = new HashMap<>();
        claims.put("accountId", accountId);

        String token = Jwts.builder().setClaims(claims).setSubject(accountId).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + TimeUnit.HOURS.toMillis(8)))
                .signWith(SignatureAlgorithm.HS512, jwtSecret).compact();

        return token;
    }

    public static String getAccountId(String token) {
        return (String) Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody().get("accountId");
    }

    public static boolean isTokenValid(String token) {
        if(token == null || token.isEmpty())
            return false;
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
            return true;
        } catch(Exception e) {
            return false;
        }
    }

    public static String extractTokenFromRequest(HttpServletRequest request) {
        String headerValue = request.getHeader(AUTH_HEADER);
        if(headerValue == null || headerValue.isEmpty() || !headerValue.contains("Bearer "))
            return null;
        return headerValue.split("Bearer ")[1];
    }



}
