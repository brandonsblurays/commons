package com.brandonsblurays.entity.account;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection="Accounts")
public class Account {
    @Id
    private String accountId;
    private String password;
}
