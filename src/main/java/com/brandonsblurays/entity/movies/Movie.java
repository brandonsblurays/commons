package com.brandonsblurays.entity.movies;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document(collection="Movies")
public class Movie {
    @Id
    private String movieId;

    //extended movie details id will be correlated to the movie api ID
    private String extendedDetailsId;

    private String accountId;

    private String movieTitle;
    private String movieRating;

    private String movieImage;

    private String linkToBuy;

    private boolean ultraHD;
    private boolean bluRay;
    private boolean dvd;

    //used for sorting movies
    private int sortRanking;

    private Date uploadDate;
}
