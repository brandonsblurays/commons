package com.brandonsblurays.entity.movies.extended;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection="MovieExtendedDetails")
@Data
public class MovieExtendedDetails {

    //extended movie details id will be correlated to the movie api ID
    @Id
    private String extendedDetailsId;

    private String movieImage;

    private String releaseDate;

    private List<String> stars;
    private List<String> writers;
    private List<String> directors;

    private String revenue;

    private String imDbRating;
    private String metacriticRating;

    private String runtimeString;
}